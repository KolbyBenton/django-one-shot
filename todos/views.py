from django.shortcuts import render, get_object_or_404
from todos.models import TodoList
# Create your views here.
def todo_list_list(request):
    todo = TodoList.objects.all()
    context = {
        "todo_object": todo,
    }
    return render(request, "todos/list.html", context)
